package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class InterestActivity extends Activity implements OnClickListener{
	float interestRate;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest);
		
				
		Button b1 = (Button)findViewById(R.id.btnCal);
		b1.setOnClickListener(this);
		
		Button b2 = (Button)findViewById(R.id.btnSetting);
		b2.setOnClickListener(this);
		
		TextView intRate = (TextView)findViewById(R.id.intRate);
		interestRate = Float.parseFloat(intRate.getText().toString());
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.interest, menu);
		return true;
	}

	public void onClick(View v) {
		int id = v.getId();
		
		if (id == R.id.btnCal) {
			EditText p = (EditText)findViewById(R.id.p);
			EditText year = (EditText)findViewById(R.id.year);
			TextView amount = (TextView)findViewById(R.id.amount);
			String res = "";
			try {
				float pusd = Float.parseFloat(p.getText().toString());
				float y = Float.parseFloat(year.getText().toString());
				double thb = (pusd * Math.pow((1+ interestRate/100),y)) ;
				res = String.format(Locale.getDefault(), "%.2f", thb);
			} catch(NumberFormatException e) {
				res = "Invalid input";
			} catch(NullPointerException e) {
				res = "Invalid input";
			}
			amount.setText(res);
		}
		else if (id == R.id.btnSetting) {
			Intent i = new Intent(this, SettingActivity.class);
			i.putExtra("interestRate", interestRate);
			startActivityForResult(i, 9999);
			// 9999 = a req code , it is a unique int val for inter
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			interestRate = data.getFloatExtra("interestRate", 10.0f);
			TextView intRate = (TextView)findViewById(R.id.intRate);
			intRate.setText(String.format(Locale.getDefault(), "%.2f", interestRate));
		}
	}

	
}
